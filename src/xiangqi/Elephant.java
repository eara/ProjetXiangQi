package xiangqi;

import javax.swing.ImageIcon;

/**
 * 
 * @author Miguel Croteau
 *
 */

public class Elephant extends Piece{

	// Data
	Position[] superPosition;
	
	/**
	 * @param nom Nom de la pièce
	 * @param couleur Couleur de la pièce
	 */
	public Elephant(String nom, String couleur) {
		super(nom, couleur);
		superPosition = new Position[7];
		// J'ai choisi de codé les super de l'éléphant en dur pour 3 raisons:
		//    1: Peu de possition possible
		//    2: Par conséquant, prend peu de temps à codé
		//    3: Donne du code très clair.
		if( couleur.equals("noir")){ // ligne 0, 2, 4   col: 0 2 4 6 8
			superPosition[0] = new Position(0, 2);
			superPosition[1] = new Position(2, 0);
			superPosition[2] = new Position(4, 2);
			superPosition[3] = new Position(2, 4);
			superPosition[4] = new Position(0, 6);
			superPosition[5] = new Position(2, 8);
			superPosition[6] = new Position(4, 6);
			this.setIcon( new ImageIcon("icones/elephantNoir.png"));
		}
		else{
			superPosition[0] = new Position(9, 2);
			superPosition[1] = new Position(7, 0);
			superPosition[2] = new Position(5, 2);
			superPosition[3] = new Position(7, 4);
			superPosition[4] = new Position(5, 6);
			superPosition[5] = new Position(7, 8);
			superPosition[6] = new Position(9, 6);
			this.setIcon(new ImageIcon("icones/elephantRouge.png"));
		}
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		if(depart.equals(arrivee))
			return true;
		
		if(norme(depart, arrivee) == 8){
			for (Position position : superPosition) {
				if(arrivee.getColonne() == position.getColonne() && arrivee.getLigne() == position.getLigne())
					return true;
			}
		}
		return false;
	}
	
}
