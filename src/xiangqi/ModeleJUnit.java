package xiangqi;

import static org.junit.Assert.*;



import org.junit.Before;
import org.junit.Test;


public class ModeleJUnit {
	
	Roi rN;
	Roi rR;
	Mandarin mN;
	Mandarin mR;
	Elephant eN;
	Elephant eR;
	Cavalier cN;
	Cavalier cR;
	Char tN;
	Char tR;
	Pion pN;
	Pion pR;
	Bombarde b;


@Before
public void initialisation()
{
	rN = new Roi("rN", "noir");
	rR = new Roi("rR", "rouge");
	mN = new Mandarin("mN", "noir");
	mR = new Mandarin("mR", "rouge");
	eN = new Elephant("eN", "noir");
	eR = new Elephant("eR", "rouge");
	cN = new Cavalier("cN", "noir");
	cR = new Cavalier("cR", "rouge");
	tN = new Char("tN", "noir");
	tR = new Char("tR", "rouge");
	pN = new Pion("pN", "noir");
	pR = new Pion("pR", "rouge");
	b = new Bombarde("b", "noir");
}
// 1
@Test
public void roiNoirTrue(){
	Position depart = new Position(0, 4);
	Position arrivee = new Position(1, 4);
	assertEquals(true, rN.estValide(depart, arrivee));
}

// 2
@Test
public void roiRougefalse(){
	Position depart = new Position(9, 5);
	Position arrivee = new Position(9, 6);
	assertEquals(false, rR.estValide(depart, arrivee));
}

// 3
@Test
public void mandarinNoirTrue(){
	Position depart = new Position(0, 3);
	Position arrivee = new Position(1, 4);
	assertEquals(true, mN.estValide(depart, arrivee));
}

// 4
@Test
public void mandarinRougeFalse(){
	Position depart = new Position(9, 5);
	Position arrivee = new Position(8, 6);
	assertEquals(false, mR.estValide(depart, arrivee));
}

// 5
@Test
public void elephantNoirTrue(){
	Position depart = new Position(0, 2);
	Position arrivee = new Position(2, 0);
	assertEquals(true, eN.estValide(depart, arrivee));
}

// 6
@Test
public void elephantRougeFalse(){
	Position depart = new Position(9, 3);
	Position arrivee = new Position(5, 3);
	assertEquals(false, eR.estValide(depart, arrivee));
}

// 7
@Test
public void cavalierNoirTrue(){
	Position depart = new Position(0, 1);
	Position arrivee = new Position(2, 2);
	assertEquals(true, cN.estValide(depart, arrivee));
}

// 8
@Test
public void cavalierRougeFalse(){
	Position depart = new Position(9, 1);
	Position arrivee = new Position(7, 3);
	assertEquals(false, cR.estValide(depart, arrivee));
}

// 9
@Test
public void charNoirTrue(){
	Position depart = new Position(0, 0);
	Position arrivee = new Position(0, 4);
	assertEquals(true, tN.estValide(depart, arrivee));
}

// 10
@Test
public void charRougeFalse(){
	Position depart = new Position(9, 1);
	Position arrivee = new Position(7, 4);
	assertEquals(false, tR.estValide(depart, arrivee));
}
// 11
@Test
public void pionNoirTrue(){
	Position depart = new Position(3, 0);
	Position arrivee = new Position(3, 0);
	assertEquals(true, pN.estValide(depart, arrivee));
}

// 12
@Test
public void PionRougeLatteralPreRiviere(){
	Position depart = new Position(4, 0);
	Position arrivee = new Position(4, 1);
	assertEquals(true, pR.estValide(depart, arrivee));
}

//13
@Test
public void PionNoirLatteralPostRiviere(){
	Position depart = new Position(5, 0);
	Position arrivee = new Position(5, 1);
	assertEquals(true, pN.estValide(depart, arrivee));
}

//14
@Test
public void bombardeTrue(){
	Position depart = new Position(2, 1);
	Position arrivee = new Position(5, 1);
	assertEquals(true, b.estValide(depart, arrivee));
}

//15
@Test
public void bombardeFalse(){
	Position depart = new Position(2, 1);
	Position arrivee = new Position(5, 3);
	assertEquals(false, b.estValide(depart, arrivee));
}

}
