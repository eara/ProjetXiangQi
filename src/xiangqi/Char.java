package xiangqi;

import javax.swing.ImageIcon;

/**
 * 
 * @author Miguel Croteau
 *
 */

public class Char extends Piece {
	
	/**
	 * @param nom Nom de la pièce
	 * @param couleur Couleur de la pièce
	 */
	// Constructor
	public Char(String nom, String couleur) {
		super(nom, couleur);
		if(couleur.equals("noir"))
			this.setIcon( new ImageIcon("icones/charNoir.png"));
		else
			this.setIcon(new ImageIcon("icones/charRouge.png"));
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		if(depart.getColonne() == arrivee.getColonne())
			return true;
		else if(depart.getLigne() == arrivee.getLigne())
			return true;
		else 
			return false;
	}
	
	
	
}
