package xiangqi;

import javax.swing.ImageIcon;

/** 
 * classe abstraite Piece servant de point de départ pour tous les types de pièces d'un jeu de Xiangqi
 * @author Éric Labonté
 * @version 3
*/

public abstract class Piece {

/**
 * nom de la pièce selon les conventions
 */
private String nom;
/**
 * couleur de la pièce
 */
private String couleur;

/**
 * En ce qui concerne la variable suivante,
 * il � �t� convenu qu'il n'�tait pas ad�quoit
 * de m�langer logique et interface utilisateur.
 * 
 * Pour les besoins du travail pratique, � d�fault
 * d'avoir les capacit�es pour cod� des interfaces graphique
 * ad�quoite, il a �t� convenue 
 * (cours du 2 d�cembre 2016 avec �ric Labont�)
 *  d'inclure l'image dans les pi�ces corespondates.
 */
private ImageIcon icon;


// constructeur permettant d'initialiser le nom et la couleur d'un objet Piece

public Piece (String nom, String couleur)
{
setNom(nom);
setCouleur ( couleur );
}

public String getNom ()
  {
  return nom;
  }

public void setNom (String nom)
  {
  this.nom = nom;
  }

public String getCouleur ()
  {
  return couleur;
  }

public void setCouleur ( String couleur )
  {
  if (( couleur == "noir" ) || ( couleur == "rouge" ))
    this.couleur = couleur;
  }


/**
 * Voir le commentaire lors de la d�claration de la variabls.
 */
public ImageIcon getIcon(){
	return icon;
}

public void setIcon(ImageIcon icon){
	this.icon = icon;
}
 
/** 
*méthode permettant de calculer la norme mathématique entre deux Positions
* @param depart Position de départ
* @param arrivee Position d'arrivée de la Pièce
* @return la somme des carrés des distances
* 
*/
public double norme (Position depart, Position arrivee)
  {
  return Math.pow((depart.getLigne()-arrivee.getLigne()), 2)+ Math.pow((depart.getColonne() - arrivee.getColonne()),2);
  }

/* méthode abstraite à implémenter dans chacune des sous - classes */

/**
 * 
 * @param depart Position de départ
 * @param arrivee Position d'arrivé
 * @return boolean Indique si le déplacement théorique est valide.
 */
public abstract boolean estValide (Position depart, Position arrivee);


}
