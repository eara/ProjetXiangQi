package xiangqi;

import javax.swing.ImageIcon;

/**
 * 
 * @author Miguel Croteau
 *
 */

public class Roi extends Piece{
	
	/**
	 * @param nom Nom de la pièce
	 * @param couleur Couleur de la pièce
	 */
	// consturcteur
	public Roi(String nom, String couleur) {
		super(nom, couleur);
		if(couleur.equals("noir"))
			this.setIcon( new ImageIcon("icones/roiNoir.png"));
		else
			this.setIcon(new ImageIcon("icones/roiRouge.png"));
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		if(depart.equals(arrivee))
			return true;
		
		if( norme(depart, arrivee) == 1){
			if(arrivee.getColonne() >= 3 && arrivee.getColonne() <= 5) // À l'intérieur du palais vertical
				if(this.getCouleur().equals("noir")){ // Noir
					if(arrivee.getLigne() >= 0 && arrivee.getLigne() <= 2) // À l'intérieur du palais Horizontale noir
						return true;
				}
				else{ // rouge
					if(arrivee.getLigne() >= 7 && arrivee.getLigne() <= 9) // À l'intérieur du palais Horizontale rouge
						return true;
				}
		}
		return false;
	}
	
	
}
