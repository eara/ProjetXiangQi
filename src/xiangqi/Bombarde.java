package xiangqi;

import javax.swing.ImageIcon;

public class Bombarde extends Piece{

	public Bombarde(String nom, String couleur) {
		super(nom, couleur);
		if(couleur.equals("noir"))
			this.setIcon( new ImageIcon("icones/bombardeNoir.png"));
		else
			this.setIcon(new ImageIcon("icones/bombardeRouge.png"));
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		if(depart.getColonne() == arrivee.getColonne())
			return true;
		else if(depart.getLigne() == arrivee.getLigne())
			return true;
		else 
			return false;
	}

	
}
