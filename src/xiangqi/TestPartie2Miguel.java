/**
 * @author Miguel Croteau
 * 
 * Je code ces teste pour valid� des proc�d�s qui ne sont pas test� par: TestPartie2Prof
 */

package xiangqi;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestPartie2Miguel {

	Roi rNoir;
	Roi rRouge;
	Bombarde b1;
	Cavalier c1;
	Elephant e1;
	Echiquier e;


@Before
public void initialisation()
{
	rNoir = new Roi("rNoir", "noir");
	rRouge = new Roi("rRouge", "rouge");
	b1 = new Bombarde("b1", "noir");
	c1 = new Cavalier("c1", "rouge");
	e1 = new Elephant("e1", "rouge");
	
	e = new Echiquier();
	e.getIntersection(0, 4).setPiece(rNoir);
	e.getIntersection(9, 4).setPiece(rRouge);
	e.getIntersection(1, 4).setPiece(c1);
	e.getIntersection(0, 3).setPiece(b1);
}

@Test 
public void roiMangePieceEntreAutreRoi(){
	Position depart = new Position(0, 4);
	Position arrivee = new Position(1, 4);
	
	assertEquals(false, e.roisNePouvantPasEtreFaceAFace(depart, arrivee));
}

@Test 
public void roiMangePieceEntreAutreRoi2(){
	Position depart = new Position(0, 4);
	Position arrivee = new Position(1, 4);
	e.getIntersection(7, 4).setPiece(e1);
	
	assertEquals(true, e.roisNePouvantPasEtreFaceAFace(depart, arrivee));
}

@Test 
public void roiSeDeplaceFaceAuRoi(){
	e.getIntersection(9, 5).setPiece(rRouge);
	e.getIntersection(9, 4).setPiece(null);
	
	Position depart = new Position(0, 4);
	Position arrivee = new Position(0, 5);
	
	assertEquals(false, e.roisNePouvantPasEtreFaceAFace(depart, arrivee));
}

@Test 
public void mangerUnAlie(){
	Position depart = new Position(0, 4);
	Position arrivee = new Position(0, 3);
	
	assertEquals(false, e.cheminPossible(depart, arrivee));
}

@Test 
public void bombardeQuiSauteSansManger(){
	Position depart = new Position(0, 3);
	Position arrivee = new Position(0, 6);
	
	assertEquals(false, e.cheminPossible(depart, arrivee));
}

@Test 
public void bombardeQuiMange(){
	e.getIntersection(1, 3).setPiece(c1);
	e.getIntersection(1, 4).setPiece(null);
	e.getIntersection(7, 3).setPiece(rRouge);
	e.getIntersection(9, 4).setPiece(null);
	
	Position depart = new Position(0, 3);
	Position arrivee = new Position(7, 3);
	
	assertEquals(true, e.cheminPossible(depart, arrivee));
}

}
