 package xiangqi;

/**
 * @author Miguel Croteau
 */

public class Echiquier implements MethodesEchiquier{
	
	// Constante
	public static final int LIGNE_MAX = 10;
	public static final int COLONE_MAX = 9;
	// Data
	private Intersection[][] jeu;
	
	public Echiquier(){
		jeu = new Intersection[LIGNE_MAX][COLONE_MAX];
		for (int i = 0 ; i < LIGNE_MAX ; i++) {
			for (int j = 0 ; j < COLONE_MAX ; j++) {
				jeu[i][j] = new Intersection();
			}
		}		
	}

	@Override
	public void debuter() {
		jeu[0][0].setPiece(new Char("t1", "noir"));
		jeu[0][8].setPiece(new Char("t2", "noir"));
		jeu[9][0].setPiece(new Char("t1", "rouge"));
		jeu[9][8].setPiece(new Char("t2", "rouge"));
		
		jeu[0][1].setPiece(new Cavalier("c1", "noir"));
		jeu[0][7].setPiece(new Cavalier("c2", "noir"));
		jeu[9][1].setPiece(new Cavalier("c1", "rouge"));
		jeu[9][7].setPiece(new Cavalier("c2", "rouge"));
		
		jeu[0][2].setPiece(new Elephant("e1", "noir"));
		jeu[0][6].setPiece(new Elephant("e2", "noir"));
		jeu[9][2].setPiece(new Elephant("e1", "rouge"));
		jeu[9][6].setPiece(new Elephant("e2", "rouge"));
		
		jeu[0][3].setPiece(new Mandarin("m1", "noir"));
		jeu[0][5].setPiece(new Mandarin("m2", "noir"));
		jeu[9][3].setPiece(new Mandarin("m1", "rouge"));
		jeu[9][5].setPiece(new Mandarin("m2", "rouge"));
		
		jeu[2][1].setPiece(new Bombarde("b1", "noir"));
		jeu[2][7].setPiece(new Bombarde("b2", "noir"));
		jeu[7][1].setPiece(new Bombarde("b1", "rouge"));
		jeu[7][7].setPiece(new Bombarde("b2", "rouge"));
		
		jeu[0][4].setPiece(new Roi("r", "noir"));
		jeu[9][4].setPiece(new Roi("r", "rouge"));
		
		int j = 1;
		for(int i = 0; i < 9 ; i += 2){
			jeu[3][i].setPiece(new Pion("p" + j, "noir"));
			jeu[6][i].setPiece(new Pion("p" + j, "rouge"));
			j++;
		}
	}

	@Override
	public Intersection getIntersection(int ligne, int colonne) {
		return jeu[ligne][colonne];
	}

	@Override
	public boolean cheminPossible(Position depart, Position arrivee) {
		// Si il ne bouge pas
		if(depart.equals(arrivee))
			return true;
		
		// Si la bombarde mange
		if(jeu[depart.getLigne()][depart.getColonne()].getPiece() instanceof Bombarde
		&& jeu[arrivee.getLigne()][arrivee.getColonne()].estOccupee())
			return siBombardeMangeCorrectement(depart, arrivee);
			
		int diffLigne = arrivee.getLigne() - depart.getLigne();
		int diffColone = arrivee.getColonne() - depart.getColonne();
		
		// Déplacement vertical
		if(diffLigne != 0 && diffColone == 0)
			return siColisionVertical(depart, arrivee);
		
		// Déplacement Horizontal
		else if(diffLigne == 0 && diffColone != 0)
			return siColisionHorizontal(depart, arrivee);
		
		// Déplacement diagonale
		if(Math.abs(diffLigne) == Math.abs(diffColone)){	
			// Déplacement Diagonale origine (0,0)
			if(diffLigne == diffColone)
				return siColisionDiagonaleOrigine(depart, arrivee);
			else // Déplacement diagonale oppsé à l'origine
				return siColisionDiagonaleOppose(depart, arrivee);
		}
		
		// Si Cavalier
		if(jeu[depart.getLigne()][depart.getColonne()].getPiece() instanceof Cavalier)
			return siColisionCavalier(depart, arrivee);
		
		return false;
	}

	@Override
	public boolean roisNePouvantPasEtreFaceAFace(Position depart, Position arrivee) {
		boolean roiQuiMange = false;
		//boolean siNoir = jeu[depart.getLigne()][depart.getColonne()].getPiece().getCouleur().equals("noir");
		
		// Ces variable deviennent vrai lorsqu'un roi est ren
		boolean roiNoir = false;
		boolean roiRouge = false;
		
		// Déplacement d'un roi
		if(jeu[depart.getLigne()][depart.getColonne()].getPiece() instanceof Roi){
			
			int diffligne = arrivee.getLigne() - depart.getLigne();
			
			// Roi qui mange
			if(jeu[arrivee.getLigne()][arrivee.getColonne()].estOccupee())
				roiQuiMange = true;
			
			// Vers le haut
			if(diffligne < 0){
				if(roiQuiMange)
					arrivee.setLigne(arrivee.getLigne() - 1);
				
				for(int it = arrivee.getLigne() ; it >= 0 ; it--)
					if(jeu[it][arrivee.getColonne()].estOccupee())
						if(jeu[it][arrivee.getColonne()].getPiece() instanceof Roi)
							return true;
						else
							return false;
				return false;
			}
			//vers le bas
			else{
				if(roiQuiMange)
					arrivee.setLigne(arrivee.getLigne() + 1);
				
				for(int i = arrivee.getLigne() ; i < LIGNE_MAX ; i++)
					if(jeu[i][arrivee.getColonne()].estOccupee())
						if(jeu[i][arrivee.getColonne()].getPiece() instanceof Roi)
							return true;
						else 
							return false;
				
				return false;
			}
		}
		// Déplacement autre que le roi
		else{
			if(arrivee.getColonne() - depart.getColonne() == 0) // D�placement sur la m�me ligne donc, le roi ne sont pas face � face
				return false;
						
			//int k = depart.getColonne() - 1;
			//int j = depart.getColonne() + 1;
			int L = depart.getColonne();
			
			for(int k = depart.getLigne() - 1; k > -1 ; k--){
				if(jeu[k][L].estOccupee()){
					if(jeu[k][L].getPiece() instanceof Roi)
						roiNoir = true;
					else
						break;
				}
			}
			
			for(int j = depart.getLigne() + 1; j < Echiquier.LIGNE_MAX ; j++){
				if(jeu[j][L].estOccupee()){
					if(jeu[j][L].getPiece() instanceof Roi)
						roiRouge = true;
					else
						break;
				}
			}
			/*while(k != -1 && j != -1){
				
				if(k >= 0 && ! roiNoir)
					if(jeu[depart.getLigne()][k].getPiece() instanceof Roi)
						roiNoir = true;
				
				if(j < LIGNE_MAX && ! roiRouge)
					if(jeu[depart.getLigne()][j].getPiece() instanceof Roi)
						roiRouge = true;
				
				k--;
				j++;
			}*/
		// sera vrai si les 2 roi se retrouvent face � face	
		}
		return roiNoir && roiRouge;
	}
	
	@Override
	public Piece manger(Position depart, Position arrivee){
		Piece tmp = jeu[arrivee.getLigne()][arrivee.getColonne()].getPiece();
		jeu[arrivee.getLigne()][arrivee.getColonne()].setPiece(jeu[depart.getLigne()][arrivee.getColonne()].getPiece());
		jeu[depart.getLigne()][depart.getColonne()].setPiece(null);
		return tmp;
	}
	
	/**
	 * Fonctions pour vérifier les colisions.
	 * Ces fonctions ont pour but de rendre la lecture de cheminPossible plus agréable.
	 * Il y en a quatre et elle sont regroupé en direction. Ces 4 direction sont représenté
	 * comme des vecteur matématique:
	 * vertical : haut et bas
	 * horizontal : gauche et droite
	 * origine: haut gauche et bas droit
	 * oppose: haut droit et bas gauche
	 */
	 
	 private boolean siColisionVertical(Position depart, Position arrivee){
		int diffligne = arrivee.getLigne() - depart.getLigne();
		// Vers le haut
		if(diffligne < 0){
			for(int i = depart.getLigne() - 1; i > arrivee.getLigne(); i--)
				if(jeu[i][depart.getColonne()].estOccupee())
					return false;
		}
		// Vers le bas
		else{
			for(int i = depart.getLigne() + 1; i < arrivee.getLigne(); i++)
				if(jeu[i][depart.getColonne()].estOccupee())
					return false;
		}
		return true;
	}
	
	private boolean siColisionHorizontal(Position depart, Position arrivee){
		int diffColone = arrivee.getColonne() - depart.getColonne();
		// Vers la gauche
		if(diffColone < 0){
			for(int i = depart.getColonne() - 1; i > arrivee.getColonne(); i--){
				if(jeu[depart.getLigne()][i].estOccupee())
					return false;
			}
		}
		// Vers la droite
		else{
			for(int i = depart.getColonne() + 1; i < arrivee.getColonne(); i++){
				if(jeu[depart.getLigne()][i].estOccupee())
					return false;
			}
		}
		return true;
	}
	
	private boolean siColisionDiagonaleOrigine(Position depart, Position arrivee){
		// Vers l'origine (0,0)
		if(arrivee.getColonne() - depart.getColonne() < 0){
			for(int i = depart.getLigne() - 1; i > arrivee.getLigne(); i--)
				for(int j = depart.getColonne() - 1; j > arrivee.getColonne(); j--)
					if(jeu[i][j].estOccupee())
						return false;
		}
		// Vers le coin bas droite (9,8)
		else{
			for(int i = depart.getLigne() + 1; i < arrivee.getLigne(); i++)
				for(int j = depart.getColonne() + 1; j < arrivee.getColonne(); j++)
					if(jeu[i][j].estOccupee())
						return false;
		}
		return true;
	}
	
	private boolean siColisionDiagonaleOppose(Position depart, Position arrivee){
		// Vers haut droit (0,8)
		if(arrivee.getColonne() - depart.getColonne() > 0){
			for(int i = depart.getLigne() - 1; i > arrivee.getLigne(); i--)
				for(int j = depart.getColonne() + 1; j < arrivee.getColonne(); j++)
					if(jeu[i][j].estOccupee())
						return false;
		}
		// vers bas gauche (9,0)
		else{
			for(int i = depart.getLigne() + 1; i < arrivee.getLigne(); i++)
				for(int j = depart.getColonne() - 1; j > arrivee.getColonne(); j--)
					if(jeu[i][j].estOccupee())
						return false;
		}
		return true;
	}
	
	private boolean siColisionCavalier(Position depart, Position arrivee){
		int diffLigne = arrivee.getLigne() - depart.getLigne();
		int diffCol = arrivee.getColonne() - depart.getColonne();
		
		// Vers le haut
		if(diffLigne == -2)
			if(jeu[depart.getLigne() - 1][depart.getColonne()].estOccupee())
				return false;
		// Vers le bas
		if(diffLigne == 2)
			if(jeu[depart.getLigne() + 1][depart.getColonne()].estOccupee())
				return false;
		// Vers la gauche
		if(diffCol == -2)
			if(jeu[depart.getLigne()][depart.getColonne() - 1].estOccupee())
				return false;
		// Vers la droite
		if(diffCol == 2)
			if(jeu[depart.getLigne()][depart.getColonne() + 1].estOccupee())
				return false;
		
		return true;
	}
	
	private boolean siBombardeMangeCorrectement(Position depart, Position arrivee){
		int diffligne = arrivee.getLigne() - depart.getLigne();
		int diffColone = arrivee.getColonne() - depart.getColonne();
		int nbColision = 0;
		
		// Vers le haut
		if(diffligne < 0)
			for(int i = depart.getLigne() - 1; i > arrivee.getLigne(); i--)
				if(jeu[i][depart.getColonne()].estOccupee())
					nbColision++;
		
		// Vers le bas
		if(diffligne > 0)
			for(int i = depart.getLigne() + 1; i < arrivee.getLigne(); i++)
				if(jeu[i][depart.getColonne()].estOccupee())
					nbColision++;
		
		// Vers la gauche
		if(diffColone < 0)
			for(int i = depart.getColonne() - 1; i > arrivee.getColonne(); i--)
				if(jeu[depart.getLigne()][i].estOccupee())
					nbColision++;
		
		// Vers la droite
		if(diffColone > 0)
			for(int i = depart.getColonne() + 1; i < arrivee.getColonne(); i++)
				if(jeu[depart.getLigne()][i].estOccupee())
					nbColision++;

		return nbColision == 1;	
	}
}
