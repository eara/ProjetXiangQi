package xiangqi;

import javax.swing.ImageIcon;

/**
 * 
 * @author Miguel Croteau
 *
 */

public class Pion extends Piece{
	
	// Contructeur
	/**
	 * @param nom Nom de la pièce
	 * @param couleur Couleur de la pièce
	 */	
	public Pion(String nom, String couleur) {
		super(nom, couleur);
		if(couleur.equals("noir"))
			this.setIcon( new ImageIcon("icones/pionNoir.png"));
		else
			this.setIcon(new ImageIcon("icones/pionRouge.png"));
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		boolean crossedRiver = false;
		
		// Remise en place
		if(depart.equals(arrivee))
			return true;
		
		// Vérifie si la revière est traversé selon la couleur de la pièce
		// noir
		if(this.getCouleur().equals("noir") && depart.getLigne() >= 5)
			crossedRiver = true;
		// rouge
		if(this.getCouleur().equals("rouge") && depart.getLigne() <= 4)
			crossedRiver = true;
		
		// Vérifie que le déplacement n'est que de 1
		if(norme(depart, arrivee) == 1){
			// Déplacement uniquement par en avant
			if(this.getCouleur().equals("noir") && depart.getLigne() < arrivee.getLigne()) // NOIR
				return true;
			else if( this.getCouleur().equals("rouge") && depart.getLigne() > arrivee.getLigne()) // ROUGE
				return true;
			// Si la rivière est traversé, déplacement latéral autorisé
			if(crossedRiver && (depart.getColonne() != arrivee.getColonne()) )
					return true;
		}
				
		return false;
	}
}
