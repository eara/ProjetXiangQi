package xiangqi;

import javax.swing.ImageIcon;

/**
 * 
 * @author Miguel Croteau
 *
 */

public class Cavalier extends Piece{
	
	// Contructeur
	/**
	 * @param nom Nom de la pièce
	 * @param couleur Couleur de la pièce
	 */
	public Cavalier(String nom, String couleur) {
		super(nom, couleur);
		if(couleur.equals("noir"))
			this.setIcon( new ImageIcon("icones/cavalierNoir.png"));
		else
			this.setIcon(new ImageIcon("icones/cavalierRouge.png"));
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		if(depart.equals(arrivee))
			return true;
		
		if ( norme(depart, arrivee) == 5){
			return true;
		}
		return false;
	}

	
}
