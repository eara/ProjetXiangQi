package xiangqi;

/**
 * @author Miguel Croteau
 */

public class Intersection {
	// Data
	Piece p;
	
	/**
	 * @param void Piece = null
	 */
	public Intersection(){
		this.p = null;
	}
	
	/**
	 * @param p Mémorise une pièce sur l'intersection
	 */
	public Intersection(Piece p){
		this.p = p;
	}
	
	/**
	 * @param p Mémorise une pièce sur l'intersection
	 */
	public void setPiece(Piece p) {
		this.p = p;
	}

	/**
	 * @return Piece Retroune la piece à cette intersection et null si il n'y a aucune pièces
	 */
	public Piece getPiece() {
		return p;
	}
	
	public boolean estOccupee(){
		if(p != null)
			return true;
		return false;
	}
	
	
}
