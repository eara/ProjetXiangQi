package xiangqi;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
//import javax.swing.JOptionPane;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.LineBorder;



import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
//import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.UIManager;

public class FrameXiangQi extends JFrame {

	private JPanel contentPane;
	JPanel panelConteneur;
	JLabel labelImage, labelCouleur;
	JLabel grille[][]; //90 JLabels transparents s'apparentant aux intersections
	JPanel panelControle;
	JButton boutonDebuter, boutonRecommencer;
	Ecouteur ec;
	Echiquier echiquier; //Échiquier faisant le lien avec la logique du jeu
	
	//Onglet Design
	JPanel panelRetraitNoir;
	JPanel panelRetraitRouge;
	
	// Retraits
	ArrayList<JLabel> iconRetraitNoir;
	ArrayList<JLabel> iconRetraitRouge;
	JLabel backgroundNoir;
	JLabel backgroundRouge;
	
	//cursor
	Toolkit tk;
	Cursor curseur;
	Image imageCursor;
	
	// icone pour debug
	ImageIcon pieceVide;
	
	//jPanel pour les retrait
	// Onglet Designe -> clic droit sur la classe.
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameXiangQi frame = new FrameXiangQi();
					frame.setVisible(true);
					frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 *constructeur
	 */
	public FrameXiangQi() {
		
		echiquier = new Echiquier(); //création de l'échiquier et des 90 JLabels
		grille = new JLabel[Echiquier.LIGNE_MAX][Echiquier.COLONE_MAX];
		
		//Nécessaire
		setTitle("XiangQi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1065, 890);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 228, 196));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		// Panel Grille
		panelConteneur= new JPanel();
		panelConteneur.setBackground(new Color(255, 228, 196));
		panelConteneur.setBounds(27, 79, 670, 769);
		panelConteneur.setLayout(new GridLayout(10, 9, 0, 0));
		panelConteneur.setOpaque(false);
		contentPane.add(panelConteneur);
		
		labelImage= new JLabel("");
		labelImage.setBounds(30, 111, 690, 700);
		contentPane.add(labelImage);
		labelImage.setIcon(( new ImageIcon( "icones/fond2.png")));
		
		panelControle = new JPanel();
		panelControle.setBackground(new Color(255, 228, 196));
		panelControle.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panelControle.setBounds(0, 11, 1039, 58);
		contentPane.add(panelControle);
		panelControle.setLayout(null);
		
		boutonRecommencer = new JButton("Recommencer");
		boutonRecommencer.setBounds(877, 22, 152, 23);
		boutonRecommencer.setBackground(new Color(255,239,213));
		boutonRecommencer.setContentAreaFilled(false);
		boutonRecommencer.setOpaque(true);
		panelControle.add(boutonRecommencer);
		boutonRecommencer.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		boutonDebuter = new JButton("D\u00E9buter");
		boutonDebuter.setBackground(new Color(255, 239, 213));
		boutonDebuter.setBounds(707, 22, 160, 23);
		boutonDebuter.setContentAreaFilled(false);
		boutonDebuter.setOpaque(true);
		panelControle.add(boutonDebuter);
		boutonDebuter.setFont(new Font("Tahoma", Font.BOLD, 15));
		labelCouleur = new JLabel("");
		labelCouleur.setBackground(new Color(255, 239, 213));
		labelCouleur.setOpaque(true);
		labelCouleur.setBounds(29, 11, 668, 41);
		
		panelControle.add(labelCouleur);
		labelCouleur.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		//Panel Noir
		panelRetraitNoir = new JPanel();
		panelRetraitNoir.setBounds(748, 127, 210, 332);
		panelRetraitNoir.setLayout(new GridLayout(5, 3, 0, 0));
		panelRetraitNoir.setOpaque(false);
		contentPane.add(panelRetraitNoir);
		
		//Panel Rouge
		panelRetraitRouge = new JPanel();
		panelRetraitRouge.setBounds(748, 497, 233, 351);
		panelRetraitRouge.setLayout(new GridLayout(5, 3, 15, 0));
		panelRetraitRouge.setOpaque(false);
		contentPane.add(panelRetraitRouge);
		
		//Backgroud des retraits
		backgroundNoir = new JLabel("");
		backgroundNoir.setBounds(733, 87, 286, 369);
		backgroundNoir.setIcon(new ImageIcon("icones/retraitBG.png"));
		contentPane.add(backgroundNoir);
		
		backgroundRouge = new JLabel("");
		backgroundRouge.setBounds(733, 471, 286, 369);
		backgroundRouge.setIcon(new ImageIcon("icones/retraitBG.png"));
		contentPane.add(backgroundRouge);
		
		// Label Ratrait
		//iconRetraitNoir = new ArrayList<JLabel>(15);
		//panelRetraitNoir.add(iconRetraitNoir);
		//iconRetraitRouge = new JLabel();
		//panelRetraitRouge.add(iconRetraitRouge);
		
		// cursor
		tk = Toolkit.getDefaultToolkit();
		pieceVide = new ImageIcon("icones/pieceVide.png");
		
		// Label des retrait
		iconRetraitNoir = new ArrayList<JLabel>(15);
		iconRetraitRouge = new ArrayList<JLabel>(15);
		for (int i = 0 ; i < 15 ; i++) {
			// noir
			iconRetraitNoir.add(new JLabel());
			panelRetraitNoir.add(iconRetraitNoir.get(i));
			iconRetraitNoir.get(i).setIcon(null);
			// rouge
			iconRetraitRouge.add(new JLabel());
			panelRetraitRouge.add(iconRetraitRouge.get(i));
			iconRetraitRouge.get(i).setIcon(null);
		}
		/*
		iconRetraitRouge = new Vector<JLabel>(14);
		for (JLabel icon : iconRetraitRouge) {
			icon = new JLabel();
			panelRetraitRouge.add(icon);
			icon.setIcon(null);
		}
		*/
		
		//gestion des évènements 
		ec = new Ecouteur();
		for ( int i = 0 ; i < Echiquier.LIGNE_MAX ; i ++ )
			for ( int j = 0 ; j < Echiquier.COLONE_MAX ; j ++ )
			{
				grille[i][j] = new JLabel();
				grille[i][j].addMouseListener( ec );
				panelConteneur.add( grille[i][j]);
				grille[i][j].setHorizontalAlignment(SwingConstants.CENTER);
			}
		boutonDebuter.addMouseListener(ec);
		boutonRecommencer.addMouseListener(ec);
	
		
	

	}
	
	// Creation de méthodes dans la classe FrameXiangQi
	
	public String changementDeTour(String s){
		if(s.equals("noir"))
			return "rouge";
		return "noir";
	}
	
	public void addPieceManger(Piece p){
		if(p.getCouleur().equals("noir"))
			for(JLabel iconSpace : iconRetraitNoir)
				if(iconSpace.getIcon() == null){
					iconSpace.setIcon(p.getIcon());
					break;
				}
		 else
			 for(JLabel iconSpac : iconRetraitRouge)
				 if(iconSpac.getIcon() == null){
					 iconSpac.setIcon(p.getIcon());
					 break;
				 }   
	}
	
	
	
	private class Ecouteur extends MouseAdapter
	{
		int ligneClic, colonneClic;
		Piece pieceTampon;
		Piece pieceEnlevee;
		//ImageIcon iconeTampon;
		Position depart, arrivee;
		String couleurControle; //valeur rouge ou noir ;
		
		@Override
		public void mouseReleased(MouseEvent e) {
			
			if ( e.getSource() == boutonDebuter){
			//CODER ICI #2
				echiquier.debuter();
				couleurControle = "rouge";
				labelCouleur.setText(couleurControle);
				for(int i = 0 ; i < Echiquier.LIGNE_MAX ; i++)
					for(int j = 0 ; j < Echiquier.COLONE_MAX ; j++)
						if(echiquier.getIntersection(i, j).estOccupee())
							grille[i][j].setIcon(echiquier.getIntersection(i, j).getPiece().getIcon());
			}
			else if ( e.getSource() == boutonRecommencer){
			//CODER ICI #3
				pieceTampon = null;
				panelConteneur.setCursor(Cursor.getDefaultCursor());
				labelCouleur.setText("");
				echiquier = new Echiquier();
				for ( int i = 0 ; i < Echiquier.LIGNE_MAX ; i ++ )
					for ( int j = 0 ; j < Echiquier.COLONE_MAX ; j ++ )
						grille[i][j].setIcon(null);
				
				for (JLabel icon : iconRetraitNoir) {
					icon.setIcon(null);
				}
				for(JLabel icons : iconRetraitRouge){
					icons.setIcon(null);
				}
			}
			else // il s'agit d'un label / intersection
			{
			    //trouver lequel
			    for ( int i = 0; i < Echiquier.LIGNE_MAX ; i++ )
			      for ( int j = 0; j < Echiquier.COLONE_MAX; j++ )
			        if (e.getSource() == grille[i][j]){
			          ligneClic = i;
			          colonneClic = j;
			          }
			      
			    // CODER ICI #4
			    // Les 3 cas de tampon!

			   // Prendre une pièce de sa couleur:
			   if(pieceTampon == null && echiquier.getIntersection(ligneClic, colonneClic).estOccupee()){
				   depart = new Position(ligneClic, colonneClic);
				   if(echiquier.getIntersection(depart.getLigne(), depart.getColonne()).getPiece().getCouleur().equals(couleurControle)){
					   pieceTampon = echiquier.getIntersection(depart.getLigne(), depart.getColonne()).getPiece();
					   grille[depart.getLigne()][depart.getColonne()].setIcon(null);
					   echiquier.getIntersection(depart.getLigne(), depart.getColonne());
					   
					   imageCursor = pieceTampon.getIcon().getImage();
					   curseur = tk.createCustomCursor(imageCursor, new Point(contentPane.getX() + 5, contentPane.getY() + 5), "img");
					   panelConteneur.setCursor(curseur);
				   }
				   //iconeTampon = (ImageIcon) grille[depart.getLigne()][depart.getColonne()].getIcon();
			   }
			   // Déposé une pièce:
			   else if(pieceTampon != null && ! echiquier.getIntersection(ligneClic, colonneClic).estOccupee()){
				   arrivee = new Position(ligneClic, colonneClic);
				   // Chemin téorique valide
				   // Pas de colision
				   // Roi ne pouvant être face à face
				   if(pieceTampon.estValide(depart, arrivee) && echiquier.cheminPossible(depart, arrivee) && !echiquier.roisNePouvantPasEtreFaceAFace(depart, arrivee)){
					   grille[arrivee.getLigne()][arrivee.getColonne()].setIcon(pieceTampon.getIcon());
					   echiquier.getIntersection(arrivee.getLigne(), arrivee.getColonne()).setPiece(pieceTampon);
					   echiquier.getIntersection(depart.getLigne(), depart.getColonne()).setPiece(null);

					   couleurControle = changementDeTour(couleurControle);
					   pieceTampon = null;
				   }
				   else{
					   grille[depart.getLigne()][depart.getColonne()].setIcon(pieceTampon.getIcon());
				   }
				   pieceTampon = null;
				   panelConteneur.setCursor(Cursor.getDefaultCursor());
				   labelCouleur.setText(couleurControle);
			   }
			   // Manger
			   else if(pieceTampon != null && echiquier.getIntersection(ligneClic, colonneClic).estOccupee()){
				   arrivee = new Position(ligneClic, colonneClic);
				   // Chemin téorique valide
				   // Pas de colision
				   // Roi ne pouvant être face à face
				   if(pieceTampon.estValide(depart, arrivee) && echiquier.cheminPossible(depart, arrivee) && ! echiquier.getIntersection(arrivee.getLigne(), arrivee.getColonne()).getPiece().getCouleur().equals(couleurControle) && ! echiquier.roisNePouvantPasEtreFaceAFace(depart, arrivee)){
					   if(!(echiquier.getIntersection(arrivee.getLigne(), arrivee.getColonne()).getPiece() instanceof Roi)){
						   contentPane.setCursor(Cursor.getDefaultCursor());
						   grille[arrivee.getLigne()][arrivee.getColonne()].setIcon(pieceTampon.getIcon());
						   
						   pieceEnlevee = echiquier.getIntersection(arrivee.getLigne(), arrivee.getColonne()).getPiece();
						   echiquier.getIntersection(arrivee.getLigne(), arrivee.getColonne()).setPiece(pieceTampon);
						   echiquier.getIntersection(depart.getLigne(), depart.getColonne()).setPiece(null);
						   //pieceTampon = echiquier.manger(depart, arrivee);
						   couleurControle = changementDeTour(couleurControle);
						   
						   // Mettre la piece manger d
						   if(pieceEnlevee.getCouleur().equals("noir")){
								for(JLabel iconSpace : iconRetraitNoir)
									if(iconSpace.getIcon() == null){
										iconSpace.setIcon(pieceEnlevee.getIcon());
										break;
									}
						   }
						   else{
								 for(JLabel iconSpac : iconRetraitRouge)
									 if(iconSpac.getIcon() == null){
										 iconSpac.setIcon(pieceEnlevee.getIcon());
										 break;
									 }
						   }
					   }
					   else{ // Roi Manger!!!!
						   labelCouleur.setText("Victoire des " + couleurControle);
						   JOptionPane.showMessageDialog(FrameXiangQi.this, "Victoire des " + couleurControle + "!!");
					   }
				   }
				   else{
					   grille[depart.getLigne()][depart.getColonne()].setIcon(pieceTampon.getIcon());
				   }
				   pieceTampon = null;
				   panelConteneur.setCursor(Cursor.getDefaultCursor());
				   labelCouleur.setText(couleurControle);
			   }
			
			} // fin label grille
			
			// Ces bouble sont pour rafraichir la grille apres un clic
			/*for(int i = 0 ; i < Echiquier.LIGNE_MAX ; i++){
				for(int j = 0 ; j < Echiquier.COLONE_MAX ; j++){
					if(echiquier.getIntersection(i, j).estOccupee() && grille[i][j].getIcon() == null){
						grille[i][j].setIcon(pieceVide);
					}
					if(! echiquier.getIntersection(i, j).estOccupee()){
						grille[i][j].setIcon(null);
					}
					else{
						grille[i][j].setIcon(echiquier.getIntersection(i, j).getPiece().getIcon());
					}
				}
			}*/
	    }// fin de la méthode mouseReleased
	}// fin de la classe Ecouteur
}


			
		
		
	

